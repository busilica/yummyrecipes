package com.example.daca.yummyrecipes.util.views;

import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.danilo.skyadapters.recycler.RvActivity;
import com.danilo.skyadapters.recycler.RvHolder;
import com.example.daca.yummyrecipes.activities.DetailsActivity;
import com.example.daca.yummyrecipes.Recipes;
import com.example.daca.yummyrecipes.util.DBHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ttlnisoffice on 1/30/18.
 */

public class UniversalOnBind {

    private RvActivity a;

    public UniversalOnBind(RvActivity a) {
        this.a = a;
    }

    public void mainOnBind(final RvHolder rvHolder, final DBHelper dbHelper, final Recipes r) {

        final String title = r.getTitle();
        final String image = r.getImage();
        final String id = r.getId();
        final String source = r.getSourceUrl();

        Picasso.with(a).load(image).into(((ImageView)rvHolder.views[2]));
        ((TextView) rvHolder.views[0]).setText(title);

        rvHolder.views[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(a, DetailsActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("title", title);
                intent.putExtra("image", image);
                intent.putExtra("source", source);
                a.startActivity(intent);
            }
        });
        if (!dbHelper.getRow(id)){
            rvHolder.views[1].setTag("off");
            ((ImageButton) rvHolder.views[1]).setImageResource(android.R.drawable.btn_star_big_off);
        }else {
            rvHolder.views[1].setTag("on");
            ((ImageButton) rvHolder.views[1]).setImageResource(android.R.drawable.btn_star_big_on);
        }

        rvHolder.views[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rvHolder.views[1].getTag().equals("off")){
                    ((ImageButton) rvHolder.views[1]).setImageResource(android.R.drawable.btn_star_big_on);
                    rvHolder.views[1].setTag("on");
                    dbHelper.insertRow(r);
                }else if (rvHolder.views[1].getTag().equals("on")){
                    ((ImageButton) rvHolder.views[1]).setImageResource(android.R.drawable.btn_star_big_off);
                    rvHolder.views[1].setTag("off");
                    dbHelper.deleteRow(r.getId());
                }
            }
        });
    }

    public void detailsOnBind(final RvHolder holder, int position, ArrayList<String> arrayList) {

        Intent intent = a.getIntent();

        ViewGroup.MarginLayoutParams marginLayoutParams =
                (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
        marginLayoutParams.setMargins(0, 0, 0, 0);

        if (position == 0){
            holder.views[1].setVisibility(View.GONE);
            holder.views[2].setVisibility(View.GONE);
            holder.views[3].setVisibility(View.GONE);
            String title = intent.getStringExtra("title");
            ((TextView) holder.views[0]).setText(title);
        }else if (position == 1) {
            holder.views[0].setVisibility(View.GONE);
            holder.views[2].setVisibility(View.GONE);
            holder.views[3].setVisibility(View.GONE);
            String image = intent.getStringExtra("image");
            Picasso.with(a).load(image).into(((ImageView) holder.views[1]));
        }else if (position == arrayList.size()+2){
            holder.views[1].setVisibility(View.GONE);
            //holder.title.setVisibility(View.GONE);
            holder.views[2].setVisibility(View.GONE);
            final String source = intent.getStringExtra("source");
            /*holder.title.setText(Html.fromHtml("<u>Directions</u>"));
            holder.title.setTextColor(Color.BLUE);
            holder.image.setVisibility(View.GONE);*/
            String directions = "Follow directions at: \n" + source;
            ((TextView) holder.views[0]).setText("Follow directions at:");
            ((TextView) holder.views[3]).setText(source);
            holder.views[3].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(a, Uri.parse(source));
                }
            });
        }else {
            holder.views[0].setVisibility(View.GONE);
            holder.views[1].setVisibility(View.GONE);
            holder.views[3].setVisibility(View.GONE);
            String in = arrayList.get(position-2);
            ((TextView) holder.views[2]).setText(in);
            //holder.title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }
    }
}
