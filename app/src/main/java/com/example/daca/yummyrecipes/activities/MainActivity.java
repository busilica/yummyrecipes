package com.example.daca.yummyrecipes.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.danilo.skyadapters.VolleyGet;
import com.danilo.skyadapters.recycler.EndlessRecyclerOnScrollListener;
import com.danilo.skyadapters.recycler.RvActivity;
import com.danilo.skyadapters.recycler.RvAdapter;
import com.danilo.skyadapters.recycler.RvHolder;
import com.danilo.skyadapters.recycler.RvInterface;
import com.danilo.skyadapters.recycler.pojo.ToolbarPOJO;
import com.danilo.skyadapters.recycler.pojo.ToolbarWithSpinnerPOJO;
import com.example.daca.yummyrecipes.R;
import com.example.daca.yummyrecipes.Recipes;
import com.example.daca.yummyrecipes.util.DBHelper;
import com.example.daca.yummyrecipes.util.JsonParser;
import com.example.daca.yummyrecipes.util.views.UniversalOnBind;

import java.util.ArrayList;

public class MainActivity extends RvActivity implements RvInterface.ToolbarCustomizer,
                                                        VolleyGet.OnResponse,
                                                        RvInterface.EndlessRvOnScrollListener {

    private String selection = "";

    @Override
    public ToolbarPOJO customizeToolbar() {
        ArrayList<String> spinnerItems = new ArrayList();
        spinnerItems.add("Trending");
        spinnerItems.add("Best Ratings");
        spinnerItems.add("Saved Recipes");
        return new ToolbarWithSpinnerPOJO(
                Color.parseColor("#c1262d"),
                spinnerItems,
                R.layout.spinner_item,
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                        String selectedItem = parent.getItemAtPosition(position).toString();
                        String url = "";
                        MainActivity.this.list = null;
                        if (selectedItem.contains("Saved Recipes")) {
                            MainActivity.this.list = null;
                            ArrayList<Recipes> recipes;
                            recipes = new DBHelper(MainActivity.this).getAllRecipes();
                            populateRv(recipes);
                        } else {
                            if (selectedItem.contains("Trending")){
                                selection = "&sort=t";
                                url = getResources().getString(R.string.ROOT) + selection;
                            } else {
                                selection = "&sort=r";
                                url = getResources().getString(R.string.ROOT) + selection;
                            }
                            new VolleyGet(MainActivity.this, url, MainActivity.this);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) { }
                }
        );
    }

    @Override
    public void parseData(String s) { populateRv(new JsonParser().parsePosts(s)); }

    @Override
    public ArrayList<Integer> getRvCustomRow_holderIDS() {
        ArrayList<Integer> l = new ArrayList<>();
        l.add(R.layout.custom_row_main);
        return l;
    }

    @Override
    public RvAdapter.RvAdapterInterface getRvOnBind() {
        return new RvAdapter.RvAdapterInterface() {
            @Override
            public void onBindViewHolder(RvHolder holder, int position) {
                new UniversalOnBind(MainActivity.this).mainOnBind(holder, new DBHelper(MainActivity.this), (Recipes)list.get(position));
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_search:
                AlertDialog.Builder search = new AlertDialog.Builder(this);
                search.setTitle("Find recipes");
                final EditText edtSearch = new EditText(this);
                search.setView(edtSearch);
                search.setPositiveButton("SEARCH", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String edt = edtSearch.getText().toString().toLowerCase().trim();
                        if (edt.contains(" ")){
                            edt = edt.replaceAll(" ", "+");
                        }
                        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                        intent.putExtra("edt", edt);
                        startActivity(intent);

                    }
                });
                search.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                search.show();
                return true;
            default:
                return false;
        }
    }

    @Override
    public EndlessRecyclerOnScrollListener onScroll() {
        return new EndlessRecyclerOnScrollListener(rv) {
            @Override
            public void onLoadMore(int i) {
                String url = getResources().getString(R.string.ROOT) + selection + "?page=" +  String.valueOf(i);
                new VolleyGet(MainActivity.this, url, MainActivity.this);
            }
        };
    }
}
