package com.example.daca.yummyrecipes.util;

import com.example.daca.yummyrecipes.Recipes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Daca on 03.05.2017..
 */

public class JsonParser {

    public ArrayList<Recipes> parsePosts(String s) {
        ArrayList<Recipes> list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("recipes");

            for (int i = 0; i < array.length(); i++){
                JSONObject object2 = array.getJSONObject(i);
                String title = object2.getString("title");
                String image = object2.getString("image_url");
                String id = object2.getString("recipe_id");
                String source = object2.getString("source_url");
                list.add(new Recipes(title, image, id, source));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<String> parseDetails(String s) {
        ArrayList<String> list = new ArrayList<>();

        try {
            JSONObject object1 = new JSONObject(s);
            JSONObject object2 = object1.getJSONObject("recipe");
            JSONArray array = object2.getJSONArray("ingredients");

            for (int i = 0; i < array.length() ; i++) {
                String ingredient = array.getString(i);
                list.add(ingredient);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}
