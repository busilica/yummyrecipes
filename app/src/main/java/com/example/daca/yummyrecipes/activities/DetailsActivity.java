package com.example.daca.yummyrecipes.activities;

import android.graphics.Color;

import com.danilo.skyadapters.VolleyGet;
import com.danilo.skyadapters.recycler.RvActivity;
import com.danilo.skyadapters.recycler.RvAdapter;
import com.danilo.skyadapters.recycler.RvHolder;
import com.danilo.skyadapters.recycler.RvInterface;
import com.danilo.skyadapters.recycler.pojo.ToolbarPOJO;
import com.danilo.skyadapters.recycler.pojo.ToolbarWithUpPOJO;
import com.example.daca.yummyrecipes.R;
import com.example.daca.yummyrecipes.util.JsonParser;
import com.example.daca.yummyrecipes.util.views.UniversalOnBind;

import java.util.ArrayList;


public class DetailsActivity extends RvActivity implements RvInterface.ToolbarCustomizer,
                                                           RvInterface.NetworkOperation {

    @Override
    public ToolbarPOJO customizeToolbar() {
        return new ToolbarWithUpPOJO(
                Color.parseColor("#c1262d"),
                getIntent().getStringExtra("title"),
                null,
                Color.WHITE,
                R.drawable.arrow_back
        );
    }

    @Override
    public VolleyGet connect() {
        return new VolleyGet(
                DetailsActivity.this,
                getResources().getString(R.string.recipe_details) + "&rId=" + getIntent().getStringExtra("id"),
                new VolleyGet.OnResponse() {
                    @Override
                    public void parseData(String s) {
                        DetailsActivity.this.populateRv(new JsonParser().parseDetails(s));
                    }
                }
        );
    }

    @Override
    public ArrayList<Integer> getRvCustomRow_holderIDS() {
        ArrayList<Integer> l = new ArrayList<>();
        l.add(R.layout.custom_row_details);
        return l;
    }

    @Override
    public RvAdapter.RvAdapterInterface getRvOnBind() {
        return new RvAdapter.RvAdapterInterface() {
            @Override
            public void onBindViewHolder(RvHolder holder, int position) {
                new UniversalOnBind(DetailsActivity.this).detailsOnBind(holder, position, (ArrayList<String>) list);
            }
        };
    }
}
