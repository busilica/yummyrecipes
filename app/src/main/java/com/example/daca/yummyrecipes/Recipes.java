package com.example.daca.yummyrecipes;

/**
 * Created by Daca on 03.05.2017..
 */

public class Recipes {

    private String title;
    private String image;
    private String id;
    private String sourceUrl;

    public Recipes(String title, String image, String id, String sourceUrl) {
        this.title = title;
        this.image = image;
        this.id = id;
        this.sourceUrl = sourceUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }
}
