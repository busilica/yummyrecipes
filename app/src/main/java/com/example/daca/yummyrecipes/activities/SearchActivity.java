package com.example.daca.yummyrecipes.activities;

import android.graphics.Color;
import android.widget.Toast;

import com.danilo.skyadapters.VolleyGet;
import com.danilo.skyadapters.recycler.EndlessRecyclerOnScrollListener;
import com.danilo.skyadapters.recycler.RvActivity;
import com.danilo.skyadapters.recycler.RvAdapter;
import com.danilo.skyadapters.recycler.RvHolder;
import com.danilo.skyadapters.recycler.RvInterface;
import com.danilo.skyadapters.recycler.pojo.ToolbarPOJO;
import com.danilo.skyadapters.recycler.pojo.ToolbarWithUpPOJO;
import com.example.daca.yummyrecipes.R;
import com.example.daca.yummyrecipes.Recipes;
import com.example.daca.yummyrecipes.util.DBHelper;
import com.example.daca.yummyrecipes.util.JsonParser;
import com.example.daca.yummyrecipes.util.views.UniversalOnBind;

import java.util.ArrayList;


public class SearchActivity extends RvActivity implements RvInterface.ToolbarCustomizer,
                                                          RvInterface.NetworkOperation,
                                                          VolleyGet.OnResponse,
                                                          RvInterface.EndlessRvOnScrollListener {

    private String url = "";

    @Override
    public ToolbarPOJO customizeToolbar() {
        return new ToolbarWithUpPOJO(
                Color.parseColor("#c1262d"),
                getIntent().getStringExtra("edt"),
                null,
                Color.WHITE,
                R.drawable.arrow_back
        );
    }

    @Override
    public VolleyGet connect() {
        url = getResources().getString(R.string.ROOT) + "&q=" + SearchActivity.this.getIntent().getStringExtra("edt");
        return new VolleyGet(this, url, this);
    }

    @Override
    public void parseData(String s) {
        populateRv(new JsonParser().parsePosts(s));
    }

    @Override
    public ArrayList<Integer> getRvCustomRow_holderIDS() {
        ArrayList<Integer> l = new ArrayList<>();
        l.add(R.layout.custom_row_main);
        return l;
    }

    @Override
    public RvAdapter.RvAdapterInterface getRvOnBind() {
        return new RvAdapter.RvAdapterInterface() {
            @Override
            public void onBindViewHolder(RvHolder holder, int position) {
                new UniversalOnBind(SearchActivity.this).mainOnBind(holder, new DBHelper(SearchActivity.this), (Recipes)list.get(position));
            }
        };
    }

    @Override
    public EndlessRecyclerOnScrollListener onScroll() {
        return new EndlessRecyclerOnScrollListener(rv) {
            @Override
            public void onLoadMore(int i) {
                url = url + "?page=" +  String.valueOf(i);
                Toast.makeText(SearchActivity.this, String.valueOf(i), Toast.LENGTH_SHORT).show();
                new VolleyGet(SearchActivity.this, url, SearchActivity.this);
            }
        };
    }
}
